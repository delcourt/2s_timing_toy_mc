from ROOT import *
from array import array

f = TFile("merged_2.root")



len_all    = {}
len_unseen = {}

for kk in f.GetListOfKeys():
    name = kk.GetName()
    if "kit_length" in name:
        vcth = int(name[-3:])
        if "_unseen" in name:
            len_unseen[vcth] = f.Get(name).GetMean()
        else:
            len_all[vcth] = f.Get(name).GetMean()

x_axis = [kk for kk in len_all]
x_axis.sort()
l_a = [len_all[v] for v in x_axis]
l_u = [len_unseen[v] for v in x_axis]


g_a = TGraph(len(x_axis), array("f",x_axis), array("f",l_a))
g_u = TGraph(len(x_axis), array("f",x_axis), array("f",l_u))
leg = TLegend(0.8,0.8,0.96,0.96)

g_u.SetLineColor(kRed)
g_u.SetMarkerColor(kRed)
g_u.SetMarkerStyle(21)
g_u.SetTitle("Timing effect ?")
g_u.GetXaxis().SetTitle("VCTH")
g_u.GetYaxis().SetTitle("Stubs duration [bx]")
g_u.Draw("ALP")

g_a.SetLineColor(kBlue)
g_a.SetMarkerColor(kBlue)
g_a.SetMarkerStyle(20)
g_a.Draw("same LP")

f2 = TFile("simul.root")
g_miss = f2.Get("size_miss")
g_miss.SetLineColor(kRed)
g_miss.SetLineStyle(7)
g_miss.SetLineWidth(4)
g_miss.Draw("same L")
g_all = f2.Get("size_all")
g_all.SetLineColor(kBlue)
g_all.SetLineStyle(7)
g_all.SetLineWidth(4)
g_all.Draw("same L")

g_u.GetYaxis().SetRangeUser(1.3,1.9)
leg.AddEntry(g_a,"All KIT stubs","lp")
leg.AddEntry(g_all,"Simulation , all","l")
leg.AddEntry(g_u,"Unseen KIT stubs","lp")
leg.AddEntry(g_miss,"Simulation , unseen","l")

leg.Draw("same")
