#include "Generator.h"
#include "TRandom3.h"
#include "TH1I.h"
#include <cmath>
#define PS_TAU     12.43
#define PS_R      -12.03
#define PS_THETA   0.79

#define SHARING_FRAC 0.7439086158490367

// x + sharing_frac = end
// ((x + sharing_frac) - 1 )/SHARING_FRAC => prop in x +1 
// 1-((x + sharing_frac) - 1 )/SHARING_FRAC => prop in x



const double PS_F1 = PS_R/(6*PS_TAU*PS_TAU)*(cos(PS_THETA)+sin(PS_THETA));
const double PS_F2 = pow(PS_R,2)/(24*pow(PS_TAU,4))*(1 + cos(PS_THETA)*sin(PS_THETA));


#include <iostream>
using namespace std;


Generator::Generator(){
    _ps = new double [1000];
    for (int i = 0; i < 1000; i++){
        _ps[i] = pulse_shape_full(i*0.1);
    }
    charge_0 = new double[5];
    charge_1 = new double[5];
    
    //ENTER LANGAUS PARAMETERS HERE

    //EFFECTIVE FITS FROM BT: 
    /*pars[0] = 0.05518699276894776;
    pars[1] = 84.81520251392514;
    pars[2] = 1.7636318617036366;
    pars[3] = 23.636086165004368;
    pars[4] = 576.1370059889707; */
    //NUMBERS FROM DN-20-014
    
    const float VCTH_CONV = 156.;
    // pars[0] = 0.081;
    // pars[1] = 19978.348/VCTH_CONV;
    // pars[2] = 1231.269/VCTH_CONV;
    // pars[3] = 3343.401/VCTH_CONV;
    pars[0] = 0.0824;
    pars[1] = 19940/VCTH_CONV;
    pars[2] = 1196/VCTH_CONV;
    pars[3] = 3374/VCTH_CONV;
    //pars[4] = 580; 
    //pars[4] = 577; 
    pars[4] = 596; 

    landau = new TF1("myLandau","landau",0,300);
    landau->SetNormalized( true );
    landau->SetParameters(pars);
    norm_dist = new TF1("my_norm_dist","gaus",-10,10);
    norm_dist->SetParameters(1,0,1);
    t_offset = 1.5;// +1.5;
    jitter   = 0;// 0.5;
    start = 0;
    stop  = 1024;
    N_gaus_points     = 100;
    N_integral_points = 1024;
    
    double gprob_integral = 0;
    gprob_vec = new double[N_gaus_points];
    for (int i = 0; i < N_gaus_points; i++){
        gprob_vec[i] = norm_dist->Eval(-5+10*(i*1./N_gaus_points));
        gprob_integral += i;
    }
    for (int i = 0; i < N_gaus_points; i++)
        gprob_vec[i]/=gprob_integral;

    langaus_vcth         = new double [N_integral_points];
    langaus_pre_computed = new double [N_integral_points];


    langaus_vcth[0]         = start;
    langaus_pre_computed[0] = 0;

    for (int i = 1; i < N_integral_points; i++){
        float i_v = start + i*(stop-start)/N_integral_points;
        langaus_vcth[i]         = i_v;
        langaus_pre_computed[i] = langaus_pre_computed[i-1] + my_langaus(pars[4]-i_v);
    }
    for (int i = 1; i < N_integral_points; i++){
        langaus_pre_computed[i]*=1./langaus_pre_computed[N_integral_points-1];
    }
    r = new TRandom3();
    h_peak_meas_edep = new TH1I("peak_edep_meas","peak_edep_meas",1024,0,1024);
    h_edep_gen       = new TH1I("edep_gen","edep_gen",1024,0,1024);
}

double Generator::my_langaus(double x){
    double rv = 0;
    for (int i = 0; i < N_gaus_points; i++){
        double xx = -5 + 10*(i*1./N_gaus_points);
        rv += landau->Eval(float(x+xx*pars[3]))*gprob_vec[i];
    }
    return 10*rv;
}

double Generator::integrated_landau(double vcth){
    if (vcth < start)
        return 0;
    if (vcth >= stop)
        return 1;
    int   bin = (vcth-start)*(stop-start)*1./N_integral_points;
    float res = vcth-langaus_vcth[bin];
    if (res > 1 || res < 0)
        cout<<"ERROR, RES = "<< res<<" not in [0;1]"<<endl;
    
    if (bin > N_integral_points - 2)
        return 1;
    
    return (1-res)*langaus_pre_computed[bin]+res*langaus_pre_computed[bin+1];
    
}



double Generator::pulse_shape_full(double t,double v) {
    return 2/0.99567*v*exp(-t/PS_TAU)*pow(t/PS_TAU,2)*(0.5+PS_F1*(t-3*PS_TAU)+PS_F2*(t*t-8*PS_TAU*t + 12 * PS_TAU*PS_TAU));
}

double Generator::pulse_shape(double t,double v){
    int   bin = t*10;
    if (bin < 0 || bin > 998)
        return 0;
    double rr = t*10-bin;
    return v*(_ps[bin]*(1-rr) + _ps[bin+1]*rr);
}

double Generator::GetEdep(){
    float y_val = r->Uniform(0,1);
    int   vcth = 0;
    for (int bit_id = 10; bit_id > -1; bit_id--){
        vcth |= (1<<bit_id);
        if (integrated_landau(vcth) > y_val){
            vcth -= (1<<bit_id);
        }
    }
    return vcth;
}

Generator::~Generator(){
    delete _ps;
}

double Generator::double_draw(){
    float x_0 = GetEdep();
    float x_1 = GetEdep();
    if (x_0 > x_1)
        return x_0;
    return x_1;
}

void Generator::gen_charge(double t0){
    float edep_0 = double_draw();  
    h_edep_gen->Fill(edep_0);
    float edep_1 = double_draw();
    h_edep_gen->Fill(edep_1);

    float min_1 = 1024;
    float min_2 = 1024;
    for(int n_charges = 0; n_charges < 5; n_charges++){
        charge_0[n_charges] = pars[4]-(pars[4]-edep_0)*pulse_shape(n_charges*25 - t0);
        charge_1[n_charges] = pars[4]-(pars[4]-edep_1)*pulse_shape(n_charges*25 - t0 + t_offset + jitter*r->Gaus());
        if (charge_0[n_charges] < min_1)
            min_1 = charge_0[n_charges];
        if (charge_1[n_charges] < min_2)
            min_2 = charge_1[n_charges];
    }
    h_peak_meas_edep->Fill(min_1);
    h_peak_meas_edep->Fill(min_2);
}

void Generator::gen_event(){
    gen_charge(r->Uniform(0,25));
}

void Generator::generate_eff(int n_events){
    // Assuming eff_pre_computed exists...

    //hard coding thresh_0...
    int thresh_0 = 550;

    for (int i = 0; i < n_events; i++){
        gen_event();
        // Checking if trigger KIT 
        int s0 = 0;
        for (int bx = 0; bx < 5 ; bx++)
            if (charge_0[bx] < thresh_0)
                s0++;
        if (s0 == 0)
            continue;
        
        for (int i = 0; i < 1000; i++){
            
            float v = langaus_vcth[i];
            bool missed = true;
            for (int bx = 0; bx < 5; bx ++){
                if (charge_1[bx] < v){
                    missed = false;
                    break;
                }
            }

            float w = 0.01;
            if (missed)
                w = 1;
/*
            denominator[i]++;
            if (missed == false)
                numerator[i] += 1;

            
            n_missed[i] += w;
            size_missed[i] += w*s0;
            size_all[i] += s0;
            n_total[i] +=1;*/
        }
    }

    float   ratio[1000];
    float   ratio_eff[1000];
    float   ratio_all[1000];
    for (int i = 0; i < 1000; i++){
    /*        ratio[i] = size_missed[i]*1./n_missed[i];  
        ratio_all[i] = size_all[i]*1./n_total[i];  
        if (denominator[i] == 0)
            ratio_eff[i] = 0;
        else
            ratio_eff[i] = 0.99*numerator[i]*1./denominator[i];
            */
    }
}

double Generator::get_eff(double vcth){}

Double_t Generator::fitter_fn(Double_t * x, Double_t * par){
    // Ignoring the parameters for the moment...
    return 1;
}
double Generator::get_charge_sharing(){
    return max(0.,((r->Uniform() + SHARING_FRAC) - 1 )/SHARING_FRAC);
}