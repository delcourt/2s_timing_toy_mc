#include "TreeReader.h"
using namespace std;

float LUT[16] = {0, 1, 2, 3, 4, 5, 6, 7, 999 ,-1,-2,-3,-4,-5,-6,-7};



int TreeReader::cic_reorder(int stub_raw){
    stub_raw = stub_raw - 2;
    int stub_add = stub_raw % 254;
    int cbc_add  = (stub_raw/254) % 8;
    int cic_number  = stub_raw/2032;
    if (cic_number == 0)
        return stub_add + 254*cic_mapper[cbc_add];
    else
        return 4064 - stub_add - 254*cic_mapper[cbc_add];
}


event::event(){
    for (int i = 0; i < 4; i++){
        stub.push_back(new vector <int> );
        stub_raw.push_back(new vector <int> );
        bendCode.push_back(new vector <int>) ;
        bend.push_back(new vector <int>) ;
    }
    
}

event::~event(){}


TreeReader::TreeReader(string fName_){
    _ff = new TFile(fName_.c_str());
    _tt = (TTree *) _ff->Get("flatTree");
    event_counter = 0;
    n_entries = _tt->GetEntries();
    _tt->SetBranchAddress("stub_0"     ,&(e.stub_raw.at(0)));
    _tt->SetBranchAddress("stub_1"     ,&(e.stub_raw.at(1)));
    _tt->SetBranchAddress("stub_2"     ,&(e.stub_raw.at(2)));
    _tt->SetBranchAddress("stub_3"     ,&(e.stub_raw.at(3)));
    _tt->SetBranchAddress("bend_code_0" ,&(e.bendCode.at(0)));
    _tt->SetBranchAddress("bend_code_1" ,&(e.bendCode.at(1)));
    _tt->SetBranchAddress("bend_code_2" ,&(e.bendCode.at(2)));
    _tt->SetBranchAddress("bend_code_3" ,&(e.bendCode.at(3)));

    _tt->SetBranchAddress("module_0_status" ,&(e.module_status_0));
    _tt->SetBranchAddress("module_1_status" ,&(e.module_status_1));
    _tt->SetBranchAddress("module_2_status" ,&(e.module_status_2));
    _tt->SetBranchAddress("module_3_status" ,&(e.module_status_3));

    _tt->SetBranchAddress("bx"              ,&(e.bx));
    _tt->SetBranchAddress("bx_super_id"     ,&(e.bx_super_id));
    _tt->SetBranchAddress("IPbus_user_data" ,&(e.IPbus_user_data));


    cic_mapper[0] = 0;
    cic_mapper[1] = 1;
    cic_mapper[2] = 2;
    cic_mapper[3] = 3;
    cic_mapper[4] = 7;
    cic_mapper[5] = 6;
    cic_mapper[6] = 5;
    cic_mapper[7] = 4;



}

void TreeReader::process_event(){
    for (int i = 0; i < 4; i++){
        e.stub.at(i)->clear();
        e.bend.at(i)->clear();
        for (auto ss : *(e.stub_raw.at(i))){
            e.stub.at(i)->push_back(cic_reorder(ss));
        }
        for (auto sb :*(e.bendCode.at(i))){
            e.bend.at(i)->push_back(LUT[sb]);
        }
    }
}


TreeReader::~TreeReader(){
    _ff->Close();
    delete _ff;
}

int TreeReader::getEvent(int event_number){
    if (event_number == -1){
        if (event_counter < n_entries){
            _tt->GetEntry(event_counter);
            process_event();
            event_counter++;
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        if (event_number < n_entries){
            _tt->GetEntry(event_number);
            process_event();
            return 1;
        }
        else{
            return 0;
        }
    }
}

int TreeReader::getEntries(){
    return n_entries;
}
