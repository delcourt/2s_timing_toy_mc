#include <iostream>
#include <cmath>
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "Generator.h"
#include "TH1.h"
using namespace std;

int main(){
    cout<<"Initialisation...";
    Generator gen;
    cout<<" Done !"<<endl;



    float x[10000];
    float y[10000];
    float max_y = 0;
    for (int i = 0; i < 10000; i++){
        x[i] = -5+i*(6*25)*1./10000;
        y[i] = gen.pulse_shape(x[i],1);
        if (y[i] > max_y)
            max_y = y[i];
    }
    cout<<max_y<<endl;
    TGraph * g = new TGraph(10000,x,y);
    g->SetTitle("Pulse shape");
    g->GetXaxis()->SetTitle("Time [ns]");
    g->GetYaxis()->SetTitle("p(t) [U.A.]");
    TFile * test = new TFile("ps.root","RECREATE");
    g->Write("pulse");
    test->Close();
    

}