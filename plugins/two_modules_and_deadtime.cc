//Simulating module efficiency using other module as ref
// Latched mode with dead-time of 2.4ns.

#include <iostream>
#include <cmath>
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "Generator.h"
#include "TRandom3.h"
#include "TEfficiency.h"
#include "TH1.h"
using namespace std;

int main(){
    cout<<"Initialisation...";
    Generator gen;

    TH1 * single_draw       = new TH1I("single_draw","single_draw",1024,0,1024);
    TH1 * h_time              = new TH1I("time","time",1000,-0.5,50.5);
    TH1 * h_time_red              = new TH1I("time_red","time_red",2000,-50.5,50.5);
    // TH1 * e_seen            = new TH1I("E_seen","E_seen",1024,0,1024);
    TEfficiency * teff      = new TEfficiency("eff","eff",201,-0.125,50.125);
    float dead_time = 2.4;

    float VCTH = 40;
    auto r = new TRandom3();
    int num = 0;
    int den = 0;
    for (int i = 0; i < 1e7; i++){
        if (i%100000 == 0)
            cout<<i/10000<<endl;
        auto h0 = 600-gen.GetEdep();
        auto h1 = 600-gen.GetEdep();

        single_draw->Fill(h0);
        single_draw->Fill(h1);
        auto t0 = r->Uniform(0,50);
        h_time->Fill(t0);
        
        int hit_0_bx = 0;
        int hit_1_bx = 0;
        

        for (float t = -12.5; t < 112.5; t+=0.01){
            float t_red  = t - 25*(int)(t/25+0.5);
            h_time_red->Fill(t_red);
            if (hit_0_bx == 0 && gen.pulse_shape(t-t0,h0) > VCTH){
                if (fabs(t_red) < dead_time/2){
                    hit_0_bx = -1;
                }
                else{
                    hit_0_bx = 1 + t/25;
                }
                
            }
            if (hit_1_bx == false && gen.pulse_shape(t-t0,h1) > VCTH) {
                if (fabs(t_red) < dead_time){
                    hit_1_bx = -1;
                }
                else{
                    hit_1_bx = 1 + t/25;
                }
            }

            if (hit_0_bx && hit_1_bx)
                break;
        }
        // cout<<"T0 "<<t0<<endl;
        
        teff->Fill(hit_1_bx > 0 && hit_1_bx == hit_0_bx, t0);
        den += 1;
        num += (hit_1_bx > 0 && hit_1_bx == hit_0_bx);



    }
    cout<<num*1./den<<endl;

    TFile * test = new TFile("simul_timing.root","RECREATE");
    single_draw->Write();
    h_time->Write();
    teff->Write();
    h_time_red->Write();
    test->Close();

}