#include <iostream>
#include <cmath>
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "Generator.h"
#include "TH1.h"
using namespace std;

int main(){
    cout<<"Initialisation...";
    Generator gen;
    cout<<" Done !"<<endl;

    TH1 * size_0            = new TH1I("size_0", "size_0", 11,-0.5,10.5);
    TH1 * single_draw       = new TH1I("single_draw","single_draw",1024,0,1024);


    float thresh_0  = 550;

    float n_missed[1000];
    float size_missed[1000];
    float size_all[1000];

    float thresh[1000];
    float langaus[1000];

    float numerator[1000];
    float denominator[1000];
        
    float n_total[1000];
    float start = 440;
    float stop  = 580;
    
    for (int i = 0; i < 1000; i++){
        thresh[i]   = start + i*(stop-start)*0.001;
        langaus[i]  = gen.integrated_landau(thresh[i]);
        n_missed[i] = 0;
        size_missed[i] = 0;
        numerator[i] = 0;
        denominator[i] = 0;
        size_all[i] = 0;
    }

    


    for (int i = 0; i < 1e6; i++){
        single_draw->Fill(gen.GetEdep());
        single_draw->Fill(gen.GetEdep());
        //h_langaus->Fill(gen.GetEdep());
        gen.gen_event();

        // Checking if trigger KIT 
        int s0 = 0;
        for (int bx = 0; bx < 5 ; bx++)
            if (gen.charge_0[bx] < thresh_0)
                s0++;
        size_0->Fill(s0);
        if (s0 == 0)
            continue;
        
        for (int i = 0; i < 1000; i++){
            
            float v = thresh[i];
            bool missed = true;
            for (int bx = 0; bx < 5; bx ++){
                if (gen.charge_1[bx] < v){
                    missed = false;
                    break;
                }
            }

            float w = 0.01;
            if (missed)
                w = 1;

            denominator[i]++;
            if (missed == false)
                numerator[i] += 1;

            
            n_missed[i] += w;
            size_missed[i] += w*s0;
            size_all[i] += s0;
            n_total[i] +=1;
        }
    }

    float   ratio[1000];
    float   ratio_eff[1000];
    float   ratio_all[1000];
    for (int i = 0; i < 1000; i++){
        ratio[i] = size_missed[i]*1./n_missed[i];  
        ratio_all[i] = size_all[i]*1./n_total[i];  
        if (denominator[i] == 0)
            ratio_eff[i] = 0;
        else
            ratio_eff[i] = 0.99*numerator[i]*1./denominator[i];
    }

    TGraph * g   = new TGraph(1000,thresh, ratio);
    TGraph * g_2 = new TGraph(1000,thresh, ratio_eff);
    TGraph * g_3 = new TGraph(1000,thresh, ratio_all);
    g_2->SetTitle("Eff_simul");

    TFile * test = new TFile("simul.root","RECREATE");
    g->Write("size_miss");
    g_3->Write("size_all");
    g_2->Write("eff_simul");
    gen.h_peak_meas_edep->Write();
    gen.h_peak_meas_edep->SetLineColor(kRed);
    gen.h_edep_gen->Write();

    cout<<"Measured max Edep: "<<gen.h_peak_meas_edep->GetXaxis()->GetBinCenter(gen.h_peak_meas_edep->GetMaximumBin())<<endl;
    cout<<"Gen      max Edep: "<<gen.h_edep_gen->GetXaxis()->GetBinCenter(gen.h_edep_gen->GetMaximumBin())<<endl;
    cout<<"Single   max Edep: "<<single_draw->GetXaxis()->GetBinCenter(single_draw->GetMaximumBin())<<endl;
    size_0->Write();
    single_draw->Write();
    test->Close();

}