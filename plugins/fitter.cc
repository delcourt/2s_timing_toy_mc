#include <iostream>
#include <cmath>
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "Generator.h"
#include "TH1.h"
using namespace std;

Generator * gen;
Double_t wrapper(Double_t * x, Double_t * par){ return gen->fitter_fn(x,par);}

int main(){
    gen = new Generator();
    TF1 *func = new TF1("test",wrapper,0,1000,5);
    const float VCTH_CONV = 156.;
    double pars [5] = {0.081, 19978.348/VCTH_CONV, 1231.269/VCTH_CONV, 3343.401/VCTH_CONV, 580};
    func->SetParameters(pars);

    TFile * test = new TFile("ps.root","RECREATE");
    func->Write();
    test->Close();
}