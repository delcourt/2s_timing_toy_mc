//Simulating module efficiency using other module as ref
// Latched mode with dead-time of 2.4ns.

#include <iostream>
#include <cmath>
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "Generator.h"
#include "TRandom3.h"
#include "TEfficiency.h"
#include "TH1.h"
using namespace std;

#define CHARGE_MULT 1.026304107793392

float dead_time = 2.4;

int get_hit(Generator & gen, int prev_hit, float t, float t0, float Edep, float VCTH){
    float t_red  = t - 25*(int)(t/25+0.5);
    // cout<<t_red<<endl;
    if (prev_hit == 0 && gen.pulse_shape(t-t0,Edep) > VCTH){
        if (fabs(t_red) < dead_time/2){
            return(-1);
        }
        else{
            return(1 + t/25);
        }    
    }
    return prev_hit;
}

bool has_stub(int h00, int h01, int h10, int h11){
    if (h00 != -1){
        if (h00 == h10 || h00 == h11)
            return 1;
    }
    if (h01 != -1){
        if (h01 == h10 || h01 == h11)
            return 1;
    }
    return 0;
}

int main(){
    cout<<"Initialisation...";
    Generator gen;
    cout<<" done!"<<endl;

    TH1 * single_draw       = new TH1I("single_draw","single_draw",1024,0,1024);
    TH1 * max_edep           = new TH1I("max_edep","max_edep",1024,0,1024);
    TH1 * h_time              = new TH1I("time","time",1000,-0.5,50.5);
    TH1 * h_time_red              = new TH1I("time_red","time_red",2000,-50.5,50.5);
    // TH1 * e_seen            = new TH1I("E_seen","E_seen",1024,0,1024);
    TEfficiency * teff_100      = new TEfficiency("eff_100","eff_100",201,-0.125,50.125);
    TEfficiency * teff_hit_100  = new TEfficiency("hit_eff_100","hit_eff_100",201,-0.125,50.125);
    TEfficiency * teff_99       = new TEfficiency("eff_99","eff_99",201,-0.125,50.125);
    TEfficiency * teff_hit_99   = new TEfficiency("hit_eff_99","hit_eff_99",201,-0.125,50.125);
    TH1 * h_cs = new TH1I("charge_sharing","charge_sharing",101,-0.05,1.05);
    float dead_time = 2.4;

    float width = 290*0.23086819112/90;

    float VCTH = 46;
    float VCTH_99 = VCTH/0.99;
    auto r = new TRandom3();
    int num = 0;
    int den = 0;
    for (int i = 0; i < 1e6; i++){
        if (i%1000 == 0)
            cout<<i<<endl;
        auto h0 = CHARGE_MULT*(596-gen.GetEdep());
        auto sf = gen.get_charge_sharing();
        auto h00 = h0*(1-sf);
        auto h01 = h0*(sf);
        auto h1 = CHARGE_MULT*(596-gen.GetEdep());
        sf = gen.get_charge_sharing();
        auto h10 = h1*(1-sf);
        auto h11 = h1*(sf);

        max_edep->Fill(max(h00,h01));
        max_edep->Fill(max(h10,h11));
        h_cs->Fill(sf);
        single_draw->Fill(h00);
        single_draw->Fill(h10);
        single_draw->Fill(h01);
        single_draw->Fill(h11);
        auto t0 = r->Uniform(0,50);
        h_time->Fill(t0);
        
        int hit_0_bx_s0 = 0;
        if (h00 < VCTH)
            hit_0_bx_s0 = -1;

        int hit_0_bx_s1 = 0;
        if (h01 < VCTH)
            hit_0_bx_s1 = -1;

        int hit_1_bx_s0 = 0;
        if (h10 < VCTH)
            hit_1_bx_s0 = -1;

        int hit_1_bx_s1 = 0;
        if (h11 < VCTH)
            hit_1_bx_s1 = -1;

        int hit_0_bx_s0_99 = 0;
        if (h00 < VCTH_99)
            hit_0_bx_s0_99 = -1;

        int hit_0_bx_s1_99 = 0;
        if (h01 < VCTH_99)
            hit_0_bx_s1_99 = -1;

        int hit_1_bx_s0_99 = 0;
        if (h10 < VCTH_99)
            hit_1_bx_s0_99 = -1;

        int hit_1_bx_s1_99 = 0;
        if (h11 < VCTH_99)
            hit_1_bx_s1_99 = -1;
        

        for (float t = -12.5; t < 112.5; t+=0.01){
            float t_red  = t - 25*(int)(t/25+0.5);
            h_time_red->Fill(t_red);
            hit_0_bx_s0 = get_hit(gen, hit_0_bx_s0, t, t0 , h00, VCTH);
            hit_0_bx_s1 = get_hit(gen, hit_0_bx_s1, t, t0 , h01, VCTH);
            hit_1_bx_s0 = get_hit(gen, hit_1_bx_s0, t, t0 , h10, VCTH);
            hit_1_bx_s1 = get_hit(gen, hit_1_bx_s1, t, t0 , h11, VCTH);

            hit_0_bx_s0_99 = get_hit(gen, hit_0_bx_s0_99, t, t0 , h00, VCTH_99);
            hit_0_bx_s1_99 = get_hit(gen, hit_0_bx_s1_99, t, t0 , h01, VCTH_99);
            hit_1_bx_s0_99 = get_hit(gen, hit_1_bx_s0_99, t, t0 , h10, VCTH_99);
            hit_1_bx_s1_99 = get_hit(gen, hit_1_bx_s1_99, t, t0 , h11, VCTH_99);

            if (hit_0_bx_s0 && hit_0_bx_s1 && hit_1_bx_s0 && hit_1_bx_s1 && hit_0_bx_s0_99 && hit_0_bx_s1_99 && hit_1_bx_s0_99 && hit_1_bx_s1_99)
                break;
            
        }
        // cout<<hit_0_bx_s0<<" "<<hit_0_bx_s1<<" "<<hit_1_bx_s0<<" "<<hit_1_bx_s1<<endl;
        // cout<<"T0 "<<t0<<endl;
        teff_hit_100->Fill(hit_1_bx_s0 > 0 || hit_1_bx_s1 > 0, t0);
        teff_hit_100->Fill(hit_0_bx_s0 > 0 || hit_0_bx_s1 > 0, t0);
        teff_hit_99->Fill(hit_1_bx_s0_99 > 0 || hit_1_bx_s1_99 > 0, t0);
        teff_hit_99->Fill(hit_0_bx_s0_99 > 0 || hit_0_bx_s1_99 > 0, t0);
        bool hs = has_stub(hit_0_bx_s0, hit_0_bx_s1, hit_1_bx_s0 , hit_1_bx_s1);
        teff_100->Fill(hs, t0);
        den += 1;
        num += (hs);
        hs = has_stub(hit_0_bx_s0_99, hit_0_bx_s1_99, hit_1_bx_s0_99 , hit_1_bx_s1_99);
        teff_99->Fill(hs, t0);

    }
    cout<<num*1./den<<endl;

    TFile * test = new TFile("simul_timing_cs.root","RECREATE");
    single_draw->Write();
    h_time->Write();
    teff_100->Write();
    teff_99->Write();
    teff_hit_100->Write();
    teff_hit_99->Write();
    h_time_red->Write();
    max_edep->Write();
    h_cs->Write();
    test->Close();

}