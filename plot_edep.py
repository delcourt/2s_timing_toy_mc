from ROOT import *
from array import array
import math

CBC_TO_PLOT = [i for i in range(16)]

leg = TLegend(0.6,0.8,0.96,0.96)
ff = TFile("simul.root")
h_single = ff.Get("single_draw")
h_single.SetLineColor(kBlue+1)
h_single.GetYaxis().SetTitle("# events [A.U.]")
h_single.GetXaxis().SetTitle("Charge [VCTH]")
h_single.Draw()


h_double = ff.Get("edep_gen")
h_double.SetLineColor(kRed+1)

h_peak   = ff.Get("peak_edep_meas")
h_peak.SetLineColor(kGreen+1)

h_peak.Draw("same")
h_double.Draw("same")

leg.AddEntry(h_single,"Langaus","l")
leg.AddEntry(h_double,"Min 2 langaus", "l")
leg.AddEntry(h_peak,"Effective stub charge", "l")
leg.Draw("same")


