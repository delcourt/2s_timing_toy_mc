#pragma once
#include <iostream>
#include <vector>
#include <map>
#include "TTree.h"
#include "TFile.h"


struct event{
    event();
    ~event();

    std::vector < std::vector <int> *>       stub;
    std::vector < std::vector <int> *>       stub_raw;
    std::vector < std::vector <int> *>       bendCode;
    std::vector < std::vector <int> *>       bend;
    uint16_t module_status_0;
    uint16_t module_status_1;
    uint16_t module_status_2;
    uint16_t module_status_3;
    uint16_t bx;
    uint32_t bx_super_id;
    uint32_t IPbus_user_data;
    
};

class TreeReader{
    private:
    TTree * _tt;
    TFile * _ff;
    int     event_counter; 
    int     n_entries;
    std::map < int , int > cic_mapper;

    public:
    TreeReader(std::string file_name);
    ~TreeReader();
    int cic_reorder(int stub_raw);
    void process_event();

    int     getEvent(int event_number = -1);
    int     getEntries();
    event   e;
};
