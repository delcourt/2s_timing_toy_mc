#pragma once
#include <utility>
#include <vector>
#include "TF1.h"
#include "TRandom.h"

class Generator{
    public:
        Generator();
        ~Generator();
        double pulse_shape(double t,double v = 1.);
        double pulse_shape_full(double t,double v = 1.);
        double get_charge_sharing();
        void gen_charge(double t0);
        double * charge_0;
        double * charge_1;
        int    size_0 = 5;
        int    size_1 = 5;
        
        double my_langaus(double x);
        double integrated_landau(double vcth);
        void   gen_event();
        double GetEdep();
        double double_draw();
        Double_t fitter_fn(Double_t * x, Double_t * par);
        TH1 * h_peak_meas_edep;
        TH1 * h_edep_gen;
        void generate_eff(int n_events);
        double get_eff(double vcth);
    private:
        double * _ps;
        TRandom * r;
        //Landau stuff
        double pars[5];
        TF1 * landau;
        TF1 * norm_dist;
        double start, stop;
        int N_gaus_points,N_integral_points;
        double * gprob_vec;
        double * langaus_vcth ;
        double * langaus_pre_computed;
        double * eff_pre_computed;
        float t_offset;
        float jitter;
};