from ROOT import *
from array import array
import math

CBC_TO_PLOT = [i for i in range(16)]
ff = TFile("data.root")

eff_data = {}
for kk in ff.GetListOfKeys():
    name = kk.GetName()
    if "ratio" in name:
        vcth = int(name.split("_")[-2])
        hist = ff.Get(name)

        eff_list = []
        for i in range(16):
            f1 = TF1("f1","pol0",i*254,(i+1)*254)
            hist.Fit(f1, "Q", "", i*254,(i+1)*254)
            eff_list.append([f1.GetParameter(0), f1.GetParError(0)])
        eff_data[vcth] = eff_list

x_axis = [vcth for vcth in eff_data]
x_axis.sort()
zeroes = [0 for x in eff_data]
y_axis = [ [ eff_data[vcth][cbc][0] for vcth in x_axis] for cbc in range(16) ]
y_axis_err = [ [ eff_data[vcth][cbc][1] for vcth in x_axis] for cbc in range(16) ]

graphs = []
fit_funcs = []
leg = TLegend(0.8,0.2,0.96,0.60)
first = True
maximums = []
fit_results = {}
for i in CBC_TO_PLOT:
    gg = TGraphErrors(len(x_axis), array('f', x_axis), array('f', y_axis[i]), array('f', zeroes), array('f', y_axis_err[i]))
    if i < 8:
        gg.SetLineColor(kAzure + i -4)
        gg.SetMarkerColor(kAzure + i -4)
        gg.SetMarkerStyle(20)
    else:
        gg.SetLineColor(kOrange + i - 12)
        gg.SetMarkerColor(kOrange + i - 12)
        gg.SetMarkerStyle(21)
    if first:
        first = False
        gg.SetTitle("Eff vs VCTH")
        gg.GetXaxis().SetTitle("Thresh [VCTH]")
        gg.GetYaxis().SetTitle("Stub eff estimate")
        gg.Draw("AP")
    else:
        gg.Draw("same P")
    graphs.append(gg)


    leg.AddEntry(gg,f"hyb {i//8}, cbc {i%8}","lp")
    
ff2 = TFile("simul.root")
gg2 = ff2.Get("eff_simul")
gg2.SetLineColor(kRed)
gg2.SetLineWidth(2)
leg.AddEntry(gg2,"Simulation","l")
gg2.Draw("same")
leg.Draw("same")


