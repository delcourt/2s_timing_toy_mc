from ROOT import *
from array import array


gc = []
def draw_sim(canvas):
    latex = TLatex()
    latex.SetNDC()
    latex.SetTextAngle(0)
    latex.SetTextColor(kBlack)    

    latex.SetTextFont(42)
    latex.SetTextAlign(11) 
    latex.SetTextSize(0.04)    
    latex.DrawLatex(canvas.GetLeftMargin(),1-canvas.GetTopMargin()+0.01,"Internal Simulation")
    latex.SetTextAlign(31) 
    latex.SetTextSize(0.03)    
    latex.DrawLatex(1-canvas.GetRightMargin(),1-canvas.GetTopMargin()+0.01,"Joint CMS-MUonE Beam-Test")
    gc.append(latex)

# file_in = TFile("simul_timing_latched.root")
file_in = TFile("simul_timing_cs.root")

eff_raw_100 = file_in.Get("eff_100")
eff_raw_99 = file_in.Get("eff_99")

c = TCanvas("","",1000,800)
eff_raw_99.Draw()
c.Update()
gg = eff_raw_99.GetPaintedGraph()
y_vals_99 = [y for y in gg.GetY()]

# print(sum(y_vals_100)/sum(y_vals_99))
# my_graph = TGraph(len(x_vals),array("f",x_vals),array("f",ratio_y))
# my_graph.Draw()

c0 = TCanvas("","",1000,800)
eff_raw_100.Draw()
c0.Update()
gg = eff_raw_100.GetPaintedGraph()
x_vals = [x for x in gg.GetX()]
y_vals_100 = [y for y in gg.GetY()]
eff = sum(y_vals_100)/len(x_vals)
print(eff)
c0 = TCanvas("","",1000,800)
gg.Draw()
ll = TLine(0,eff,50,eff)
ll.SetLineColor(kGreen+1)
ll.SetLineWidth(2)
ll.Draw("same")
eff_raw_100.SetTitle("")
gg.SetTitle("")
gg.GetXaxis().SetTitle("Particle arrival time [ns]")
gg.GetYaxis().SetTitle("Stub reconstruction probability")
leg = TLegend(0.15,0.2,0.35,0.4)
leg.AddEntry(gg,"Efficiency","lp")
leg.AddEntry(ll,f"Avg = {100*eff:.1f}%","l")
leg.Draw("same")
draw_sim(c0)
c0.Update()

c0.Print("Efficiency_raw.png")




step = x_vals[2]-x_vals[1]
print(f"Step = {step} ns")
t0 = (18.0218 - 18.0218)/29.979245800
t1 = (21.8693 - 18.0218)/29.979245800
t2 = (89.9218 - 18.0218)/29.979245800 - 2
t3 = (93.7693 - 18.0218)/29.979245800 - 2

# t0 = 0
# t1 = 0.1
# t2 = 2.2-2
# t3 = 2.3-2
print(t0,t1,t2,t3)
t0 = int(t0/step+0.5)
t1 = int(t1/step+0.5)
t2 = int(t2/step+0.5)
t3 = int(t3/step+0.5)

#####Getting efficiency with given time stucture:

n_time_val = len(x_vals)
eff_convoluted = []
eff_convoluted_99 = []
for delay in range(n_time_val):
    num = 0
    num_99 = 0
    den = 0
    for t_part in range(n_time_val):
        t_dut  = t_part + delay + t1
        t_dut  = t_dut%n_time_val
        t0_obs = (t_part+t0) % n_time_val
        t2_obs = (t_part+t2) % n_time_val
        t3_obs = (t_part+t3) % n_time_val
        p_ref  = y_vals_100[t0_obs]*y_vals_100[t2_obs]*y_vals_100[t3_obs]
        p_obs  = p_ref*y_vals_100[t_dut]
        p_obs_99 = p_ref*y_vals_99[t_dut]
        num += p_obs
        num_99 += p_obs_99
        den += p_ref
    eff_convoluted.append(num*1./den)
    eff_convoluted_99.append(num_99*1./den)
print(f"E0 = {eff_convoluted[0]}")
ll2 = TLine(0,eff_convoluted[0],50,eff_convoluted[0])
print(eff_convoluted[0])
ll2.SetLineColor(kGreen+1)
ll2.SetLineStyle(2)
ll2.SetLineWidth(2)
ll2.Draw("same")
leg.AddEntry(ll2,f"Observable eff = {100*eff_convoluted[0]:.1f}%","l")
# draw_sim(c0)
c0.Update()
c0.Print("Efficiency_conv.png")

c = TCanvas("","",1000,800)
gg.Draw("AP")
# gg.SetTitle("Naive simulation")
gg2 = TGraph(n_time_val, array('f',x_vals),array('f',eff_convoluted))

gg2.SetLineColor(kRed)
gg2.SetLineWidth(2)
gg.GetXaxis().SetTitle("Arrival time / delay [ns]")
gg.GetYaxis().SetTitle("Efficiency")
gg2.Draw("same")
leg = TLegend(0.15,0.15,0.5,0.4)
leg.AddEntry(gg  ,"Efficiency","lp")
leg.AddEntry(gg2 ,"Observable eff ","lp")
leg.Draw("same")
draw_sim(c)
c.Update()
c.Modified()
c.Print("Eff_with_delay.png")

gg3 = TGraph(n_time_val, array('f',x_vals),array('f',eff_convoluted_99))
gg3.SetLineColor(kBlue)
leg.AddEntry(gg3 ,f"Observable eff (Gain = 0.98)","lp")
gg3.SetLineWidth(2)

gg3.Draw("same")
ll3 = TLine(0,eff_convoluted[0],50,eff_convoluted[0])
ll3.SetLineColor(kRed)
ll3.SetLineStyle(2)
ll3.SetLineWidth(2)
leg.AddEntry(ll3 ,f"Gain= 1.00, no delay : {100*eff_convoluted[0]:.2f}%","l")
ll3.Draw("same")
ll4 = TLine(0,eff_convoluted_99[0],50,eff_convoluted_99[0])
ll4.SetLineColor(kBlue)
ll4.SetLineStyle(2)
ll4.SetLineWidth(2)
ll4.Draw("same")
leg.AddEntry(ll4 ,f"Gain = 0.98, no delay : {100*eff_convoluted_99[0]:.2f}%","l")

gg.GetYaxis().SetRangeUser(0.93,1.001)
leg.Draw("same")
c.Update()
draw_sim(c)
c.Print("simul.pdf")
c.Print("simul.png")

c3 = TCanvas("","",1000,800)
c3.SetLeftMargin(0.13)
gg4 = TGraph(n_time_val, array('f',x_vals),array('f',[200*(e99-e100)/(e99+e100) for e100,e99 in zip(eff_convoluted, eff_convoluted_99)]))
gg4.Draw()
gg4.SetTitle("")
gg4.GetXaxis().SetTitle("Delay [ns]")
gg4.GetYaxis().SetTitle("Gain 0.98 / 1.00 relative difference [%]")
draw_sim(c3)
c3.Update()
c3.Print("rel_dif.png")

cratio = TCanvas("","",1000,800)
ratio_y = [xx_100/xx_99 if xx_99 > 0 else 1 for (xx_100,xx_99) in zip(y_vals_100,y_vals_99)]
gg_ry = TGraph(len(ratio_y),array("f",x_vals),array("f", ratio_y))
gg_ry.Draw()
gg_ry.SetTitle("")
gg_ry.GetXaxis().SetTitle("Particle arrival time [ns]")
gg_ry.GetYaxis().SetTitle("Stub efficiency ratio G 1.00/G 0.98")
draw_sim(cratio)
cratio.Update()
cratio.Print("Stub_efficiency_ratio.png")
