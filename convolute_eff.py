from ROOT import *
from array import array
file_in = TFile("simul_timing.root")
eff_raw = file_in.Get("eff")

#dist:
c = TCanvas()
eff_raw.Draw()
c.Update()
gg = eff_raw.GetPaintedGraph()
x_vals = [x for x in gg.GetX()]
y_vals = [y for y in gg.GetY()]
print(x_vals)

n_time_val = len(x_vals)
eff_convoluted = []
for delay in range(n_time_val):
    num = 0
    den = 0
    for t0 in range(n_time_val):
        t1 = t0 + delay
        t1 = t1%n_time_val
        p_ref = y_vals[t0]
        p_obs = p_ref*y_vals[t1]
        num += p_obs
        den += p_ref
    eff_convoluted.append(num*1./den)
c = TCanvas("","",1000,800)
gg.Draw("AP")
gg.SetTitle("Naive simulation")
gg2 = TGraph(n_time_val, array('f',x_vals),array('f',eff_convoluted))
gg2.SetLineColor(kRed)
gg2.SetLineWidth(2)
gg.GetXaxis().SetTitle("Arrival time / delay [ns]")
gg.GetYaxis().SetTitle("Efficiency")
c.Update()
c.Modified()
gg2.Draw("same")

leg = TLegend(0.15,0.15,0.4,0.3)
leg.AddEntry(gg  ,"Single module efficiency","lp")
leg.AddEntry(gg2 ,"Observable efficiency ","lp")
leg.Draw("same")

c.Print("simul.pdf")
c.Print("simul.png")

