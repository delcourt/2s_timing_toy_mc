
_DEPS = TreeReader.h Generator.h
_OBJ = TreeReader.o Generator.o
_PLUGINS = test.out pulse_shape.out fitter.out test_timing.out two_modules_and_deadtime.out Ali_simul.out Ali_simul_cs.out

IDIR = ./include
LDIR = ./lib
ODIR = ./src
PDIR = ./plugins
BDIR = ./bin

PLUGINS = $(patsubst %,$(BDIR)/%,$(_PLUGINS))


CFLAGS = -I$(IDIR) -I./  -L$(ROOTSYS)/lib
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
OBJ  = $(patsubst %,$(ODIR)/%,$(_OBJ))
CC = g++
COpt = -Wall -Wextra  `root-config --cflags --libs` -O3

all:
	@echo "############# COMPILING ALL #############"
	@echo "###### COMPILING DEPENDENCIES..."
	@make  -j4 $(OBJ)
	@echo "###### COMPILING $(PLUGINS)..."
	@make -j8 $(PLUGINS)

%.o: %.cc
	$(CC) -c -o $@ $< $(CFLAGS) $(ROOTLIBS) -L$(ROOTSYS)/lib $(COpt) 


bin/%.out: plugins/%.cc $(OBJ)
	$(CC)  -o $@ $^ $(CFLAGS) $(ROOTLIBS) -L$(ROOTSYS)/lib $(COpt)


.PHONY: clean

clean:
	rm -rf $(ODIR)/*.o hellomake src/Dict.cc include/Dict.h bin/* Dict_rdict.pcm plugins/*.out

  
